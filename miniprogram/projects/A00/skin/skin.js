module.exports = {
	PID: 'A00', // 校庆

	NAV_COLOR: '#ffffff',
	NAV_BG: '#C6000E',

	MEET_NAME: '活动',

	MENU_ITEM: ['首页', '活动日历', '我的'], // 第1,4,5菜单
 
	NEWS_CATE: '1=校庆公告,2=校庆动态,3=校史回眸,4=校庆服务,5=校园风光,6=校友风采,7=致校友|upimg',
	MEET_TYPE: '1=校庆活动',

	DEFAULT_FORMS: [{
			type: 'line',
			title: '姓名',
			desc: '请填写您的姓名',
			must: true,
			len: 50,
			onlySet: {
				mode: 'all',
				cnt: -1
			},
			selectOptions: ['', ''],
			mobileTruth: true,
			checkBoxLimit: 2,
		},
		{
			type: 'line',
			title: '手机',
			desc: '请填写您的手机号码',
			must: true,
			len: 50,
			onlySet: {
				mode: 'all',
				cnt: -1
			},
			selectOptions: ['', ''],
			mobileTruth: true,
			checkBoxLimit: 2,
		}
	]
}