let behavior = require('../../../../behavior/default_index_bh.js');
let PassortBiz = require('../../../../biz/passport_biz.js');
let skin = require('../../skin/skin.js');

Page({
	behaviors: [behavior],

	onReady: function () {
		PassortBiz.initPage({
			skin,
			that: this,
			isLoadSkin: true,
			tabIndex: -1,
			isModifyNavColor: true
		});

		let PROJECT_CEL_DAY = '2022-12-30'
		let celDay = PROJECT_CEL_DAY.split('-');
		celDay = celDay[0] + '年' + celDay[1] + '月' + celDay[2] + '日';
		let day = this.calDay(PROJECT_CEL_DAY);
		this.setData({
			day,
			celDay
		});
	},
	calDay: function (day) {
		let arrDay = day.split('-');
		let t1 = arrDay[0] + "/" + arrDay[1] + "/" + arrDay[2];
		let dateBegin = new Date(t1);

		let date = new Date();
		let result = date.getTime() - dateBegin.getTime();
		return Math.abs(Math.floor(result / (24 * 3600 * 1000)));
	}

})